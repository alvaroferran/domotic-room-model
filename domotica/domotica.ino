#include "dht.h"


//input  
int pin_fuego=2; //está
int pin_boton=3;
int pin_pres=4; //está
int
pin_temp=14;//está
int pin_ldr=15;// o 1 en analogRead
int pin_pot=16;
int pin_agua=11;

//output
int pin_radiador=5;
int pin_lamp1=7;//está
int pin_lamp2=8;//está
int pin_motor1=9;//está
int pin_motor2=10;//está
int pin_alarma=18;//está



//variables
int interrupcion=0;
int val_ldr;
int thresh_ldr=400;
int luzFuera;
int last_luzFuera;
int cambioLuz;
int val_pres;
int persona;
int val_temp;
int thresh_temp;
int delta_temp=2;
int motor_speed=140;
int wait_motor=150; 

int wait_alarma=500;

//Temperature Object
dht DHT;

void setup(){

  pinMode(pin_fuego, INPUT);
  pinMode(pin_boton, INPUT);
  pinMode(pin_pres, INPUT);
  pinMode(pin_temp, INPUT);
  pinMode(pin_ldr, INPUT);
  pinMode(pin_pot, INPUT);
  pinMode(pin_agua, INPUT);

  pinMode(pin_radiador, OUTPUT);
  pinMode(pin_lamp1, OUTPUT);
  pinMode(pin_lamp2, OUTPUT);
  pinMode(pin_motor1, OUTPUT);
  pinMode(pin_motor2, OUTPUT);
  pinMode(pin_alarma, OUTPUT);  

  Serial.begin(9600);
  
   val_ldr=analogRead(pin_ldr);
  if (val_ldr > thresh_ldr) luzFuera=1; // hay luz
  else luzFuera=0;
  last_luzFuera=luzFuera;
  
  delay(2000);

}




void loop(){

 /* if (interrupcion==0){
    if (digitalRead(pin_fuego)==HIGH && digitalRead(pin_agua)==HIGH)  // !fire && !water
      normal();
    else{
      interrupcion=1; 
      emergencia();      
    }
  }
  else{
    emergencia();
    if (digitalRead(pin_boton)==LOW)
      interrupcion=0;
  }

*/
  /*  while(digitalRead(pin_boton)==HIGH && digitalRead(pin_fuego)==LOW){
   emergencia();
   }*/
   if (digitalRead(pin_boton)==LOW)
   {
   digitalWrite(pin_lamp1,HIGH);
   digitalWrite(pin_lamp2,LOW);
   }
   else
   {
   digitalWrite(pin_lamp2,LOW);
   digitalWrite(pin_lamp1,LOW);
   }
   /*                        analogWrite(pin_motor1, motor_speed);
   			digitalWrite(pin_motor2, LOW);
   			delay(wait_motor);
   			digitalWrite(pin_motor1, LOW);
   			digitalWrite(pin_motor2, LOW);
   
   
   delay(2000);			
   
   digitalWrite(pin_lamp1,LOW);
   digitalWrite(pin_lamp2,LOW);					//bajar
   			analogWrite(pin_motor2, motor_speed);	
   			digitalWrite(pin_motor1, LOW);
   			delay(wait_motor);
   			digitalWrite(pin_motor1, LOW);
   			digitalWrite(pin_motor2, LOW);
   
   delay(2000);*/
}



/*****NORMAL*********************************************************************************************************/

void normal(){

  //LDR
  val_ldr=analogRead(pin_ldr);
  if (val_ldr > thresh_ldr) luzFuera=1; // hay luz
  else luzFuera=0;

  if(last_luzFuera != luzFuera)		//comprobar si hay cambio en luz ambiente
    cambioLuz=1; 
  else cambioLuz=0;




  //Presencia
  val_pres=digitalRead(pin_pres);
  if(val_pres==HIGH) persona=1;
  else persona=0;


  //Temperatura
  DHT.read11(pin_temp);
  //DHT.humidity --> variable that returns the humidity measured
  //DHT.temperature --> variable that returns the temperature
  if(DHT.temperature!=0)  val_temp=DHT.temperature; //if there is a read error skip value
  thresh_temp=map(analogRead(pin_pot), 0, 1023, 15, 40);  //entre 15 y 30ºC




  //Radiador
  if(val_temp > (thresh_temp + delta_temp)) { 

    digitalWrite(pin_radiador, LOW);
    Serial.println("OFF");
    Serial.println("---");	
  }
  else if(val_temp < (thresh_temp - delta_temp)) {
    digitalWrite(pin_radiador, HIGH);
    Serial.println("ON");
    Serial.println("---");
  }


  //Lampara
  if( luzFuera==0 && persona==1) {		//lamp on
    digitalWrite(pin_lamp1, HIGH);
    digitalWrite(pin_lamp2, LOW);
  }
  else {		//lamp off
    digitalWrite(pin_lamp1, LOW);
    digitalWrite(pin_lamp2, LOW);
  }




  //Persiana

  //borrar despues esto!!!!
  cambioLuz=1; 
  luzFuera=1; 
  if(cambioLuz==1){	
    if(luzFuera==1){							//subir
      analogWrite(pin_motor1, motor_speed);
      digitalWrite(pin_motor2, LOW);
      delay(wait_motor);
      digitalWrite(pin_motor1, LOW);
      digitalWrite(pin_motor2, LOW);
    }
    else{										//bajar
      analogWrite(pin_motor2, motor_speed);	
      digitalWrite(pin_motor1, LOW);
      delay(wait_motor);
      digitalWrite(pin_motor1, LOW);
      digitalWrite(pin_motor2, LOW);
    }
  }



  last_luzFuera=luzFuera;	

}



/*****EMERGENCIA*********************************************************************************************************/

void emergencia(){
  digitalWrite(pin_alarma, HIGH);
  delay(wait_alarma);
  digitalWrite(pin_alarma, LOW);
  delay(wait_alarma);
}



